package pl.kamilmielnik.designpatterns.creational.singleton;

public class Client {

	public static void main(String[] args) {
		Singleton singleton = Singleton.getInstance();
		singleton.operation();
	}

}
