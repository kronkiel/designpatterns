package pl.kamilmielnik.designpatterns.creational.factoryMethod.creators;

import pl.kamilmielnik.designpatterns.creational.factoryMethod.model.ConcreteProduct;
import pl.kamilmielnik.designpatterns.creational.factoryMethod.model.Product;

public class ConcreteCreator extends Creator {

	@Override
	protected Product factoryMethod() {
		System.out.println("ConcreteCreator creates ConcreteProduct");
		return new ConcreteProduct();
	}

}
