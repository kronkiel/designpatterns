package pl.kamilmielnik.designpatterns.creational.abstractFactory.model;

public class ProductB1 extends AbstractProductB {

	@Override
	public void operation() {
		System.out.println("ProductB1 operation");
	}

}
