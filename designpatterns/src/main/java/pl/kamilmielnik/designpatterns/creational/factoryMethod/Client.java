package pl.kamilmielnik.designpatterns.creational.factoryMethod;

import pl.kamilmielnik.designpatterns.creational.factoryMethod.creators.ConcreteCreator;
import pl.kamilmielnik.designpatterns.creational.factoryMethod.creators.Creator;

public class Client {

	public static void main(String[] args) {
		Creator creator = new ConcreteCreator();
		creator.operation();
	}

}
