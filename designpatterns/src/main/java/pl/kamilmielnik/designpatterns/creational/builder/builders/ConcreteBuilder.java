package pl.kamilmielnik.designpatterns.creational.builder.builders;

import pl.kamilmielnik.designpatterns.creational.builder.model.Product;

public class ConcreteBuilder extends Builder {

	protected Product product;

	public ConcreteBuilder() {
		this.product = new Product();
	}

	@Override
	public void buildPartA() {
		System.out.println("ConcreteBuilder builds part A");
	}

	@Override
	public void buildPartB() {
		System.out.println("ConcreteBuilder builds part B");
	}

	public Product getResult() {
		System.out.println("ConcreteBuilder returns result");
		return product;
	}

}
