package pl.kamilmielnik.designpatterns.creational.factoryMethod.creators;

import pl.kamilmielnik.designpatterns.creational.factoryMethod.model.Product;

public abstract class Creator {

	protected Product product;

	protected abstract Product factoryMethod();

	public void operation() {
		product = factoryMethod();
	}

}
