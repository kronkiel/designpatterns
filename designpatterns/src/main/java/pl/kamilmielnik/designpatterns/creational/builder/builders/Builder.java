package pl.kamilmielnik.designpatterns.creational.builder.builders;

public abstract class Builder {

	public abstract void buildPartA();

	public abstract void buildPartB();

}
