package pl.kamilmielnik.designpatterns.creational.prototype;

public class Client {

	public static void main(String[] args) {
		Prototype prototype = new ConcretePrototype2();
		Prototype newObject = prototype.clone();
		newObject.operation();
	}

}
