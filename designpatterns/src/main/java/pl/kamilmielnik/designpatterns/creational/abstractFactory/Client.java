package pl.kamilmielnik.designpatterns.creational.abstractFactory;

import pl.kamilmielnik.designpatterns.creational.abstractFactory.model.AbstractProductA;
import pl.kamilmielnik.designpatterns.creational.abstractFactory.model.AbstractProductB;
import pl.kamilmielnik.designpatterns.creational.abstractFactory.factories.AbstractFactory;
import pl.kamilmielnik.designpatterns.creational.abstractFactory.factories.ConcreteFactory1;
import pl.kamilmielnik.designpatterns.creational.abstractFactory.factories.ConcreteFactory2;

public class Client {

	public static void main(String[] args) {
		AbstractFactory factory1 = new ConcreteFactory1();
		AbstractProductA a1 = factory1.createProductA();
		a1.operation();
		AbstractProductB b1 = factory1.createProductB();
		b1.operation();

		AbstractFactory factory2 = new ConcreteFactory2();
		AbstractProductA a2 = factory2.createProductA();
		a2.operation();
		AbstractProductB b2 = factory2.createProductB();
		b2.operation();
	}

}
