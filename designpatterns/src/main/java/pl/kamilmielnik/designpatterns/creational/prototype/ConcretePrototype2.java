package pl.kamilmielnik.designpatterns.creational.prototype;

public class ConcretePrototype2 extends Prototype {

	@Override
	public Prototype clone() {
		System.out.println("ConcretePrototype2 clones itself");
		return new ConcretePrototype2();
	}

	@Override
	public void operation() {
		System.out.println("ConcretePrototype2 operation");
	}

}
