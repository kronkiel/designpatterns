package pl.kamilmielnik.designpatterns.creational.builder.model;

public class Product {
	
	public void operation() {
		System.out.println("Product operation");
	}
	
}
