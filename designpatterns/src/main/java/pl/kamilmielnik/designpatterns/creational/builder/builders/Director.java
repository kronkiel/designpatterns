package pl.kamilmielnik.designpatterns.creational.builder.builders;

public class Director {

	protected Builder builder;
	
	public Director(Builder builder) {
		this.builder = builder;
	}
	
	public void construct() {
		builder.buildPartA();
		builder.buildPartB();
	}
	
}
