package pl.kamilmielnik.designpatterns.creational.abstractFactory.factories;

import pl.kamilmielnik.designpatterns.creational.abstractFactory.model.AbstractProductA;
import pl.kamilmielnik.designpatterns.creational.abstractFactory.model.AbstractProductB;
import pl.kamilmielnik.designpatterns.creational.abstractFactory.model.ProductA1;
import pl.kamilmielnik.designpatterns.creational.abstractFactory.model.ProductB1;

public class ConcreteFactory1 extends AbstractFactory {

	@Override
	public AbstractProductA createProductA() {
		System.out.println("ConcreteFactory1 creates Product A1");
		return new ProductA1();
	}

	@Override
	public AbstractProductB createProductB() {
		System.out.println("ConcreteFactory1 creates Product B1");
		return new ProductB1();
	}

}
