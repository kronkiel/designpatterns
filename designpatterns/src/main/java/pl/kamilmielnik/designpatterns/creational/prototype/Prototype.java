package pl.kamilmielnik.designpatterns.creational.prototype;

public abstract class Prototype {

	public abstract Prototype clone();
	
	public abstract void operation();

}
