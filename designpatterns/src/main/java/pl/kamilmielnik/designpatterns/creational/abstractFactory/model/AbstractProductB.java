package pl.kamilmielnik.designpatterns.creational.abstractFactory.model;

public abstract class AbstractProductB {

	public abstract void operation();
	
}
