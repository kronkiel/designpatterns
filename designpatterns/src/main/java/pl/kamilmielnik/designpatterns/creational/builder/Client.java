package pl.kamilmielnik.designpatterns.creational.builder;

import pl.kamilmielnik.designpatterns.creational.builder.builders.ConcreteBuilder;
import pl.kamilmielnik.designpatterns.creational.builder.builders.Director;
import pl.kamilmielnik.designpatterns.creational.builder.model.Product;

public class Client {

	public static void main(String[] args){
		ConcreteBuilder builder = new ConcreteBuilder();
		Director director = new Director(builder);
		director.construct();
		Product product = builder.getResult();
		product.operation();
	}
	
}
