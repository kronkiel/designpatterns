package pl.kamilmielnik.designpatterns.creational.abstractFactory.model;

public abstract class AbstractProductA {
	
	public abstract void operation();
	
}
