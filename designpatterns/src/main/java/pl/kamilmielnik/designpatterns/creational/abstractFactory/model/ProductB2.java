package pl.kamilmielnik.designpatterns.creational.abstractFactory.model;

public class ProductB2 extends AbstractProductB {

	@Override
	public void operation() {
		System.out.println("ProductB2 operation");
	}

}
