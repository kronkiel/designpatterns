package pl.kamilmielnik.designpatterns.creational.abstractFactory.factories;

import pl.kamilmielnik.designpatterns.creational.abstractFactory.model.AbstractProductA;
import pl.kamilmielnik.designpatterns.creational.abstractFactory.model.AbstractProductB;

public abstract class AbstractFactory {

	public abstract AbstractProductA createProductA();

	public abstract AbstractProductB createProductB();

}
