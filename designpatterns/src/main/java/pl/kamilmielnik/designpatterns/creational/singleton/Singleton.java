package pl.kamilmielnik.designpatterns.creational.singleton;

public class Singleton {

	protected static Singleton instance = null;

	public static Singleton getInstance() {
		if (instance == null) {
			System.out.println("Creating new singleton instance");
			instance = new Singleton();
		}
		System.out.println("Returning singleton instance");
		return instance;
	}

	private Singleton() {
		
	}

	public void operation() {
		System.out.println("Singleton operation");
	}

}
