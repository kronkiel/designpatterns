package pl.kamilmielnik.designpatterns.creational.abstractFactory.model;

public class ProductA1 extends AbstractProductA {

	@Override
	public void operation() {
		System.out.println("ProductA1 operation");
	}

}
