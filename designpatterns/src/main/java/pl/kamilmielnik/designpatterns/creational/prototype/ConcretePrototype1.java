package pl.kamilmielnik.designpatterns.creational.prototype;

public class ConcretePrototype1 extends Prototype {
	
	@Override
	public Prototype clone() {
		System.out.println("ConcretePrototype1 clones itself");
		return new ConcretePrototype1();
	}

	@Override
	public void operation() {
		System.out.println("ConcretePrototype1 operation");
	}

}
