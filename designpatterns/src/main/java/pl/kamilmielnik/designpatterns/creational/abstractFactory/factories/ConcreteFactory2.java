package pl.kamilmielnik.designpatterns.creational.abstractFactory.factories;

import pl.kamilmielnik.designpatterns.creational.abstractFactory.model.AbstractProductA;
import pl.kamilmielnik.designpatterns.creational.abstractFactory.model.AbstractProductB;
import pl.kamilmielnik.designpatterns.creational.abstractFactory.model.ProductA2;
import pl.kamilmielnik.designpatterns.creational.abstractFactory.model.ProductB2;

public class ConcreteFactory2 extends AbstractFactory {

	@Override
	public AbstractProductA createProductA() {
		System.out.println("ConcreteFactory2 creates Product A2");
		return new ProductA2();
	}

	@Override
	public AbstractProductB createProductB() {
		System.out.println("ConcreteFactory2 creates Product B2");
		return new ProductB2();
	}

}
