package pl.kamilmielnik.designpatterns.creational.abstractFactory.model;

public class ProductA2 extends AbstractProductA {

	@Override
	public void operation() {
		System.out.println("ProductA2 operation");
	}

}
