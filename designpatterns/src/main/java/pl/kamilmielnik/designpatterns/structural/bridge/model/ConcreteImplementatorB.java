package pl.kamilmielnik.designpatterns.structural.bridge.model;

public class ConcreteImplementatorB extends Implementor {

	@Override
	public void operationImpl() {
		System.out.println("ConcreteImplementatorB operation impl");
	}

}
