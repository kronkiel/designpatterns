package pl.kamilmielnik.designpatterns.structural.flyweight.model;

import java.util.HashMap;
import java.util.Map;

public class FlyweightFactory {

	protected Map<String, Flyweight> flyweightsMap;

	public FlyweightFactory() {
		this.flyweightsMap = new HashMap<String, Flyweight>();
	}

	public Flyweight getFlyweight(String key) {
		if (!flyweightsMap.containsKey(key)) {
			Flyweight flyweight = new ConcreteFlyweight();
			flyweightsMap.put(key, flyweight);
			return flyweight;
		}
		return flyweightsMap.get(key);
	}

}
