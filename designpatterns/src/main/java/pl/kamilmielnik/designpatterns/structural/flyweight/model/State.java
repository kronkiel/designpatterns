package pl.kamilmielnik.designpatterns.structural.flyweight.model;

public class State {

	protected String name;

	public State(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
