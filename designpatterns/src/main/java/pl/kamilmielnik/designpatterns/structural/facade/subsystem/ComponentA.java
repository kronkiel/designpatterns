package pl.kamilmielnik.designpatterns.structural.facade.subsystem;

public class ComponentA {

	public void operation1() {
		System.out.println("ComponentA operation1");
	}

	public void operation2() {
		System.out.println("ComponentA operation2");
	}
}
