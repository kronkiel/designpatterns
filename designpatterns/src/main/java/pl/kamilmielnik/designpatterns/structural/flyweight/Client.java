package pl.kamilmielnik.designpatterns.structural.flyweight;

import java.util.LinkedList;
import java.util.List;

import pl.kamilmielnik.designpatterns.structural.flyweight.model.Flyweight;
import pl.kamilmielnik.designpatterns.structural.flyweight.model.FlyweightFactory;
import pl.kamilmielnik.designpatterns.structural.flyweight.model.State;

public class Client {

	public static void main(String[] args) {
		FlyweightFactory flyweightFactory = new FlyweightFactory();
		Flyweight flyweightA = flyweightFactory.getFlyweight("a");
		Flyweight flyweightB = flyweightFactory.getFlyweight("b");
		
		List<Flyweight> list = new LinkedList<>();
		list.add(flyweightA);
		list.add(flyweightB);
		State state = new State("externalState");
		for(Flyweight flyweight : list){
			flyweight.operation(state);
		}
	}

}
