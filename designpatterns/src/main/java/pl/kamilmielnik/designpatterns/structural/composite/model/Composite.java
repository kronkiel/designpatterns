package pl.kamilmielnik.designpatterns.structural.composite.model;

import java.util.LinkedList;
import java.util.List;

public class Composite extends Component {

	protected List<Component> children;

	public Composite() {
		this.children = new LinkedList<>();
	}

	@Override
	public void operation() {
		for (Component component : children) {
			component.operation();
		}
	}

	public void add(Component component) {
		children.add(component);
	}

	public void remove(Component component) {
		children.remove(component);
	}

	public List<Component> getChildren() {
		return children;
	};

}
