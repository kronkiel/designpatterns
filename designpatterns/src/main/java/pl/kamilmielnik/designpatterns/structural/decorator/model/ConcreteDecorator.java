package pl.kamilmielnik.designpatterns.structural.decorator.model;

public class ConcreteDecorator extends Decorator {

	public ConcreteDecorator(Component component) {
		super(component);
	}

	public void operation() {
		super.operation();
		addedBehavior();
	}

	protected void addedBehavior() {
		System.out.println("ConcreteDecorator added behavior");
	}

}
