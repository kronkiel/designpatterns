package pl.kamilmielnik.designpatterns.structural.decorator.model;

public class ConcreteComponent extends Component {

	@Override
	public void operation() {
		System.out.println("Concrete component operation");
	}

}
