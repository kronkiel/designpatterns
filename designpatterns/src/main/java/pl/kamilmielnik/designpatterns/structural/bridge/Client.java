package pl.kamilmielnik.designpatterns.structural.bridge;

import pl.kamilmielnik.designpatterns.structural.bridge.model.Abstraction;
import pl.kamilmielnik.designpatterns.structural.bridge.model.ConcreteImplementatorB;
import pl.kamilmielnik.designpatterns.structural.bridge.model.Implementor;
import pl.kamilmielnik.designpatterns.structural.bridge.model.RefinedAbstraction;

public class Client {

	public static void main(String[] args) {
		Implementor implementor = new ConcreteImplementatorB();
		Abstraction abstraction = new RefinedAbstraction(implementor);
		abstraction.operation();
	}

}
