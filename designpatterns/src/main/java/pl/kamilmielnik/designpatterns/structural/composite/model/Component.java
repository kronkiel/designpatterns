package pl.kamilmielnik.designpatterns.structural.composite.model;

import java.util.LinkedList;
import java.util.List;

public abstract class Component {

	public abstract void operation();

	public void add(Component component) {
	}

	public void remove(Component component) {
	}

	public List<Component> getChildren() {
		return new LinkedList<Component>();
	};

}
