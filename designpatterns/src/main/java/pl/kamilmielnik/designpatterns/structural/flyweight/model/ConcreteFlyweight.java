package pl.kamilmielnik.designpatterns.structural.flyweight.model;

public class ConcreteFlyweight extends Flyweight {

	@Override
	public void operation(State externalState) {
		System.out.println("ConcreteFlyweight operation with state: " + externalState.getName());
	}

}
