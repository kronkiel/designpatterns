package pl.kamilmielnik.designpatterns.structural.composite.model;

public class Leaf extends Component {

	@Override
	public void operation() {
		System.out.println("Leaf operation");
	}

}
