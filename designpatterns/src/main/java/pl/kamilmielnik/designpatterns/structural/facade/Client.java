package pl.kamilmielnik.designpatterns.structural.facade;

import pl.kamilmielnik.designpatterns.structural.facade.subsystem.Facade;

public class Client {

	public static void main(String[] args) {
		Facade facade = new Facade();
		facade.operationA();
		facade.operationB();
	}

}
