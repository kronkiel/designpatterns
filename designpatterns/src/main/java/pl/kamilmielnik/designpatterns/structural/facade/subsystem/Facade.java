package pl.kamilmielnik.designpatterns.structural.facade.subsystem;

public class Facade {

	protected ComponentA componentA;
	protected ComponentB componentB;

	public Facade() {
		this.componentA = new ComponentA();
		this.componentB = new ComponentB();
	}

	public void operationA() {
		componentA.operation1();
		componentB.operation1();
	}

	public void operationB() {
		componentB.operation2();
		componentB.operation2();
	}

}
