package pl.kamilmielnik.designpatterns.structural.decorator.model;

public abstract class Component {

	public abstract void operation();
	
}
