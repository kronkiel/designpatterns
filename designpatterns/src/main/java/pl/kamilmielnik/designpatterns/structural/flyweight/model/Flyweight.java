package pl.kamilmielnik.designpatterns.structural.flyweight.model;

public abstract class Flyweight {

	public abstract void operation(State externalState);
	
}
