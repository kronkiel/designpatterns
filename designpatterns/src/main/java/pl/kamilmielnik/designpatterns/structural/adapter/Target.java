package pl.kamilmielnik.designpatterns.structural.adapter;

public abstract class Target {

	public abstract void request();

}
