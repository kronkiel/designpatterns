package pl.kamilmielnik.designpatterns.structural.bridge.model;

public class RefinedAbstraction extends Abstraction {

	public RefinedAbstraction(Implementor implementor) {
		super(implementor);
	}

	public void refinedOperation() {
		System.out.println("RefinedAbstraction refined operation");
	}

}
