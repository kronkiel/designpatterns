package pl.kamilmielnik.designpatterns.structural.proxy.model;

public class Proxy extends Subject {

	protected Subject realSubject;
	
	public Proxy(Subject subject){
		this.realSubject = subject;
	}

	@Override
	public void request() {
		realSubject.request();
	}
	
}
