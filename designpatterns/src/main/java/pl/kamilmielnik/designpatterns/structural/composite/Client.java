package pl.kamilmielnik.designpatterns.structural.composite;

import pl.kamilmielnik.designpatterns.structural.composite.model.Component;
import pl.kamilmielnik.designpatterns.structural.composite.model.Composite;
import pl.kamilmielnik.designpatterns.structural.composite.model.Leaf;

public class Client {

	public static void main(String[] args) {
		Component leaf1 = new Leaf();
		Component leaf2 = new Leaf();
		Component composite1 = new Composite();
		composite1.add(leaf1);
		composite1.add(leaf2);

		Component leaf3 = new Leaf();
		Component composite2 = new Composite();
		composite2.add(composite1);
		composite2.add(leaf3);

		composite2.operation();
	}

}
