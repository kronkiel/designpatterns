package pl.kamilmielnik.designpatterns.structural.adapter;

public class Adapter extends Target {

	protected Adaptee adaptee;

	public Adapter() {
		this.adaptee = new Adaptee();
	}

	@Override
	public void request() {
		adaptee.specificRequest();
	}
	
	

}
