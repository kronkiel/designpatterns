package pl.kamilmielnik.designpatterns.structural.bridge.model;

public abstract class Implementor {

	public abstract void operationImpl();
	
}
