package pl.kamilmielnik.designpatterns.structural.proxy.model;

public class RealSubject extends Subject {

	@Override
	public void request() {
		System.out.println("RealSubject request");
	}

}
