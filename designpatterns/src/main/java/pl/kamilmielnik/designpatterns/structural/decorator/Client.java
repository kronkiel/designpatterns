package pl.kamilmielnik.designpatterns.structural.decorator;

import pl.kamilmielnik.designpatterns.structural.decorator.model.Component;
import pl.kamilmielnik.designpatterns.structural.decorator.model.ConcreteComponent;
import pl.kamilmielnik.designpatterns.structural.decorator.model.ConcreteDecorator;

public class Client {

	public static void main(String[] args) {
		Component component = new ConcreteComponent();
		Component decoratedComponent = new ConcreteDecorator(component);
		decoratedComponent.operation();
	}

}
