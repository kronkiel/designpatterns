package pl.kamilmielnik.designpatterns.structural.proxy;

import pl.kamilmielnik.designpatterns.structural.proxy.model.Proxy;
import pl.kamilmielnik.designpatterns.structural.proxy.model.RealSubject;
import pl.kamilmielnik.designpatterns.structural.proxy.model.Subject;

public class Client {

	public static void main(String[] args) {
		Subject realSubject = new RealSubject();
		Proxy proxy = new Proxy(realSubject);
		proxy.request();
	}

}
