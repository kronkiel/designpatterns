package pl.kamilmielnik.designpatterns.structural.flyweight.model;

public class UnsharedConcreteFlyweight extends Flyweight {

	@Override
	public void operation(State state) {
		System.out.println("UnsharedConcreteFlyweight operation with state: " + state.getName());
	}

}
