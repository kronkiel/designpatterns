package pl.kamilmielnik.designpatterns.structural.bridge.model;

public class ConcreteImplementatorA extends Implementor {

	@Override
	public void operationImpl() {
		System.out.println("ConcreteImplementatorA operation impl");
	}

}
