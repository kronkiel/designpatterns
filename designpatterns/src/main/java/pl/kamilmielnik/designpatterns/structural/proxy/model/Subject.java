package pl.kamilmielnik.designpatterns.structural.proxy.model;

public abstract class Subject {

	public abstract void request();
	
}
