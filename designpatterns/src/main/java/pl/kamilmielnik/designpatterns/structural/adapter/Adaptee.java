package pl.kamilmielnik.designpatterns.structural.adapter;

public class Adaptee {

	public void specificRequest() {
		System.out.println("Adaptee specific request");
	}
	
}
