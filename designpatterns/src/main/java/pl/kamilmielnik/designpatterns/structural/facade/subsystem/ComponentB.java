package pl.kamilmielnik.designpatterns.structural.facade.subsystem;

public class ComponentB {

	public void operation1() {
		System.out.println("ComponentB operation1");
	}

	public void operation2() {
		System.out.println("ComponentB operation2");
	}

}
